import React from 'react';
import ReactDOM from 'react-dom';
import {LoginContainer} from './Containers/LoginContainer';
import { BrowserRouter, Route } from 'react-router-dom';

function App(){
    return (
    <div>
        <BrowserRouter>
            <Route exact path="/" component={LoginContainer} />
        </BrowserRouter>
    </div>
    );
}


ReactDOM.render(<App />, document.getElementById('root'));

