import React, {useForm} from 'react';

export default function LoginInput(){
    const [register, handleSubmit] = useForm();

    return (
        <form>
            <input name="username"
                   placeholder="Username"
                   ref={register} />
        </form>
    );
}

