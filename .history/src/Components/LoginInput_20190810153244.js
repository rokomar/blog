import React from 'react';
import {useForm} from 'react-hook-form';

export function LoginInput(){

    const {register, handleSubmit} = useForm();

    return (
        <form>
            <input name="username"
                   placeholder="Username"
                   ref={register}
                   />
            
            <input name="password"
                   placeholder="Password"
                   ref={register}
                   />

            <input type="submit"/>
        </form>
    );
}

