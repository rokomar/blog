import React, {useForm} from 'react';

function LoginInput(){
    const [register, handleSubmit] = useForm();

    return (
        <form>
            <input name="username"
                   placeholder="Username"
                   ref={register} />
        </form>
    );
}

export default LoginInput;