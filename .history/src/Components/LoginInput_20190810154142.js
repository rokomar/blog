import React from 'react';
import useForm from 'react-hook-form';

export function LoginInput(){

    const {register, handleSubmit} = useForm();

    return (
        <form>
            <label htmlFor="username">Username</label>
            <input name="username"
                   
                   ref={register}
                   />
            
            <input name="password"
                   placeholder="Password"
                   ref={register}
                   />

            <input type="submit"/>
        </form>
    );
}

