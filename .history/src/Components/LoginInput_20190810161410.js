import React from 'react';
import useForm from 'react-hook-form';
import Login from '../Styles/Login.module.css';

export function LoginInput(props){

    const {register, handleSubmit} = useForm();

    return (
        <form className={Login.form} onSubmit={handleSubmit(props.submit)}>

            <h1 className={Login.header}>Login</h1>
            
            <label htmlFor="username">Username</label>
            <input name="username"
                   ref={register} />
                    
            <label htmlFor="password">Password</label>
            <input name="password"
                   type="password"
                   ref={register} />

            <button type="submit">Login</button>       
                  
        </form>
    );
}

