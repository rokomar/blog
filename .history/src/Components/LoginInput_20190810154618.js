import React from 'react';
import useForm from 'react-hook-form';

export function LoginInput(props){

    const {register, handleSubmit} = useForm();

    return (
        <form>
            <label htmlFor="username">Username</label>
            <input name="username"
                   ref={register}
                   />
            
            <label htmlFor="password">Password</label>
            <input name="password"
                   type="password"
                   ref={register}
                   />

            <input type="submit"       
                   />
        </form>
    );
}

