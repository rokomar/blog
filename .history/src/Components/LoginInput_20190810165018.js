import React from 'react';
import useForm from 'react-hook-form';
import Login from '../Styles/Login.module.css';

export function LoginInput(props){

    const {register, handleSubmit, errors} = useForm();

    return (
        <form className={Login.form} onSubmit={handleSubmit(props.submit)}>

            <h1 className={Login.header}>Login</h1>
            {(errors.username || errors.password) && <span className={Login.error}>Incorrect username or password.</span>}

            <label className={Login.label} htmlFor="username">Username</label>
            <input className={Login.input}
                   name="username"
                   ref={register({
                       required: true
                   })} />
          
            <label className={Login.label} htmlFor="password">Password</label>
            <input className={Login.input}
                   name="password"
                   type="password"
                   ref={register({
                    required: true
                   })} />

            <button className={Login.button} type="submit">Login</button>       

            <span>Don't have an account?</span>
        </form>
    );
}

