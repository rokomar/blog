import React from 'react';
import {useForm} from 'react-hook-form';

export function LoginInput(){

    return (
        <form>
            <input name="username"
                   placeholder="Username"
                   />
            
            <input name="password"
                   placeholder="Password"
                   />

            <input type="submit" />
        </form>
    );
}

