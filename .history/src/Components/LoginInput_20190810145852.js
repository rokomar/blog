import React, {useForm} from 'react';

const LoginInput = () => {
    const [register, handleSubmit] = useForm();

    return (
        <form>
            <input name="username"
                   placeholder="Username"
                   ref={register} />
        </form>
    );
}

export default LoginInput;