import React from 'react';
import useForm from 'react-hook-form';
import Login from '../Styles/Login.css';

export function LoginInput(props){

    const {register, handleSubmit} = useForm();

    return (
        <form className={Login.form} onSubmit={handleSubmit(props.submit)}>

            <label htmlFor="username">Username</label>
            <input name="username"
                   ref={register} />
                    
            <label htmlFor="password">Password</label>
            <input name="password"
                   type="password"
                   ref={register} />

            <input type="submit" />       
                  
        </form>
    );
}

