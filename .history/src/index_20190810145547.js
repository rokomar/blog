import React from 'react';
import ReactDOM from 'react-dom';
import { Login } from './Containers/LoginContainer';
import { BrowserRouter, Route } from 'react-router-dom';

function App(){
    return (
    <div>
        <BrowserRouter>
            <Route exact path="/" component={Login} />
        </BrowserRouter>
    </div>
    );
}


ReactDOM.render(<App />, document.getElementById('root'));

