import React from 'react';
import Workout from '../Styles/Workout.module.css';
import styles from '../Styles/NewExercises.module.css';
import { observer } from 'mobx-react';
import edit from '../Images/edit.svg';
import close from '../Images/close.svg';

function newExercisesComponent(props){

  const handleDelete = (name) => {
    props.appState.newExercises.replace(props.appState.newExercises.filter(e => e.name !== name));
  }

  return (
    <div className={Workout.divContainer}>
      {props.appState.newExercises.map(newExercise =>
        <div className={styles.container} key={newExercise.name}>
           <span className={Workout.workout}>{newExercise.name}</span>
           <span>{newExercise.sessionDescription} </span>
           <button className={styles.button} onClick={() => handleDelete(newExercise.name)}><img src={close} alt='close' className={styles.icon}/></button>
        </div>
      )}
    </div>
  );
}

export const NewExercises = observer(newExercisesComponent)
