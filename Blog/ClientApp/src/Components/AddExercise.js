import React from 'react';
import useForm from 'react-hook-form';
import styles from '../Styles/AddExercise.module.css';
import LogReg from '../Styles/LogReg.module.css';
import { observer } from 'mobx-react';

function AddExerciseComponent(props) {

  const {register, handleSubmit} = useForm();

  return (
    <form onSubmit={handleSubmit(props.handleAdd)} className={styles.form}>
      <input name="exercises"
             className={styles.input}
             ref={register({
               required: true
             })}
             placeholder="Exercise"
             list="exercises"/>
      <datalist id="exercises">
        {props.appState.exercises.map((exercise) =>
              <option key={exercise} value={exercise} />
        )}
      </datalist>

      <input name="sessionDescription"
             className={styles.input}
             ref={register({
               required: true
             })}
             placeholder="Description" />

      <button name="botun"
              type="submit"
              className={styles.button}>Add</button>
    </form>
  );
}

export const AddExercise = observer(AddExerciseComponent);
