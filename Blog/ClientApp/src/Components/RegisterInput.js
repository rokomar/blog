import React from 'react';
import useForm from 'react-hook-form';
import styles from '../Styles/LogReg.module.css';

export function RegisterInput(props){

  const {register, handleSubmit, errors} = useForm();

  return(
    <form className={styles.form} onSubmit={handleSubmit(props.submit)}>
        <h1 className={styles.header}>Register</h1>

        {props.error && <span className={styles.error}>Username already exists</span>}

        <div className={styles.divContainer}>
        <label htmlFor="username" className={styles.label}>Username</label>
        <input name="username"
               className={styles.input}
               ref={register({
                 required: true
               })} />
        </div>

        <div className={styles.divContainer}>
        <label htmlFor="email" className={styles.label}>Email</label>
        <input name="email"
               className={styles.input}
               ref={register({ //referencira register sa ovim atributima odnosno ograničenjima
                 required: true,
                 pattern: {
                      value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                      message: 'This is not a valid email',
                 }
               })} />
        {errors.email && <div className={styles.error}>{errors.email.message}</div>}
        </div>

        <div className={styles.divContainer}>
        <label htmlFor="password" className={styles.label}>Password</label>
        <input name="password"
               type="password"
               className={styles.input}
               ref={register({
                 required: true,
                 minLength: {
                   value: 8,
                   message: 'Password has to be 8 characters or longer'
                 }
               })} />
        {errors.password && <div className={styles.error}>{errors.password.message}</div>}
        </div>

        <button type="submit" name="Register" className={styles.button}>Register</button>

        <span>Already have an account?</span>
        <a href="/" className={styles.link}>Login here.</a>
    </form>
  );
}
