import React from 'react';
import useForm from 'react-hook-form';
import LogReg from '../Styles/LogReg.module.css';
import styles from '../Styles/AddNewExercise.module.css';
import AddNewWorkout from '../Styles/AddNewWorkout.module.css';

export function AddNewExercise(props){

  const {register, handleSubmit} = useForm();


  return (
    <form className={LogReg.form} onSubmit={handleSubmit(props.handleSubmit)}>
      <span className={styles.header}>Add new exercise</span>


      <input name="exercise"
             placeholder="Exercise"
             ref={register({
               required: true
             })}
             className={styles.input}/>
      {props.error && <span className={LogReg.error}>Exercise already exists</span>}

      <textarea name="description"
                ref={register()}
                className={styles.textarea}
                rows= "3"
                cols= "21"
                placeholder="Notes"/>

      <button name="botun"
              type="submit"
              className={AddNewWorkout.button}>Submit exercise</button>
    </form>
  );
}
