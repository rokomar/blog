import React, { useState } from 'react';
import styles from '../Styles/AddNewWorkout.module.css';
import { NewExercises } from './NewExercises';
import LogReg from '../Styles/LogReg.module.css';
import { AddExercise } from '../Components/AddExercise';
import { observer } from 'mobx-react';

function AddNewWorkoutComponent(props){

  const [errorAddExercise, setErrorAddExercise] = useState(false)

  const handleAddExercise = (res) => {
      if(!props.appState.exercises.find(e => e === res.exercises)){
          setErrorAddExercise(true);
        }
      else{
        setErrorAddExercise(false);
        if(props.appState.newExercises.find(e => e.name === res.exercises)){
          props.appState.newExercises.find(e => e.name == res.exercises).sessionDescription = res.sessionDescription;
        }
        else{
          const data = {
            'date' : props.appState.date,
            'name' : res.exercises,
            'username' : localStorage.getItem('Username'),
            'sessionDescription' : res.sessionDescription
          };
          props.appState.newExercises.push(data);
       }
      }
  }

  const handleDateChange = (e) => {
    props.appState.date = e.target.value;
  }


  return (
    <div className={LogReg.form}>
      <span className={styles.header}>Add new workout</span>

      <div className={styles.container}>
        <label htmlFor="date">Pick a date </label>
        <input type="date"
               onChange={handleDateChange}
               name="date"/>

        {props.appState.date !== 0 && <AddExercise handleAdd={handleAddExercise} appState={props.appState}/>}

        {errorAddExercise  && <span className={LogReg.error}>Exercise doesn't exist. </span>}

        {props.appState.newExercises.length != 0 && [ <NewExercises appState={props.appState} handleSubmit={props.handleSubmit}/>,
                                                      <button className={styles.button} onClick={() => props.handleSubmit()}>Submit workout</button> ] }


      </div>
    </div>

  );
}

export const AddNewWorkout = observer(AddNewWorkoutComponent);
