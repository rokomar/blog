import React from 'react';
import styles from '../Styles/Header.module.css';

export function Header(props){

  return (
    <div className={styles.header}>
      <span>WorkoutApp</span>
    </div>
  );
}
