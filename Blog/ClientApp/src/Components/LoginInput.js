import React from 'react';
import useForm from 'react-hook-form';
import styles from '../Styles/LogReg.module.css';

export function LoginInput(props){

    const {register, handleSubmit} = useForm();

    return (
        <form className={styles.form} onSubmit={handleSubmit(props.submit)}>

            <h1 className={styles.header}>Login</h1>
            {props.error && <span className={styles.error}>Incorrect username or password.</span>}

            <div className={styles.divContainer}>
            <label className={styles.label} htmlFor="username">Username</label>
            <input className={styles.input}
                   name="username"
                   ref={register({
                       required: true
                   })} />
            </div>

            <div className={styles.divContainer}>
            <label className={styles.label} htmlFor="password">Password</label>
            <input className={styles.input}
                   name="password"
                   type="password"
                   ref={register({
                    required: true
                   })} />
            </div>

            <button className={styles.button} type="submit">Login</button>

            <span>Don't have an account?</span>
            <a href="/registration" className={styles.link}>Register here</a>
        </form>
    );
}
