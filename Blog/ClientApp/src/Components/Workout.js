import React from 'react';
import styles from '../Styles/Workout.module.css';
import { observer } from 'mobx-react';

function WorkoutComponent(props){

  const date = props.appState.workouts[props.data][0].date.split("T")[0];

  return (
    <div className={styles.divContainer}>
      <span className={styles.date}>{date}</span>

      {props.appState.workouts[props.data].map(workout =>
          <div className={styles.workoutDiv} key={workout.name}>
            <span className={styles.workout}>{workout.name}</span>
            <span>{workout.sessionDescription}</span>
          </div>
      )}

    </div>
  );
}

export const Workout = observer(WorkoutComponent);
