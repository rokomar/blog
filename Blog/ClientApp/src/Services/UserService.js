import { post } from './API';

export function LoginUser(dest, body, props){
    return post(dest, body).then((res) => {
      localStorage.setItem("Username", res.username);
      localStorage.setItem("Token", res.token);
      props.history.push("/workouts");
    })
}

export function RegisterUser(dest, body, props){
    return post(dest, body).then((res) => {
      props.history.replace("/");
    })
}
