export function post(dest, body){
    return fetch(dest,{
        method: 'POST',
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": " Bearer ".concat(localStorage.getItem('Token')),
        },
        body: JSON.stringify(body)
    }).then((response) => {
        if(!response.ok)
            throw new Error(response.status);
        return response.json();
    })
}

export function get(dest){
  return fetch(dest,{
     method: 'GET',
     headers: {
       "Content-Type": "application/json",
       "Accept":"application/json",
       "Authorization": " Bearer ".concat(localStorage.getItem('Token')),
     }
  }).then((response) => {
      if(!response.ok)
          throw new Error(response.status);
      return response.json();
  });
}
