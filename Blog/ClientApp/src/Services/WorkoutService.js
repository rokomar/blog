import { get, post } from './API';

export function GetWorkouts(dest, appState, renderWorkouts){
    return get(dest)
          .then((res) => {
            appState.workouts = res;
            renderWorkouts();
          })
}

export function PostNewWorkout(dest, appState){
    return post(dest, appState.newExercises)
          .then((res) => {
            console.log(res);
            appState.newExercises = [];
            appState.date = 0;
            appState.workouts.push(res);
            appState.workouts.replace(appState.workouts.slice().sort((e1, e2) => new Date(e2[0].date) - new Date(e1[0].date)));
          })
}

export function GetExercises(dest, appState){
    return get(dest)
           .then((res) => {
             appState.exercises = res.exercises;
           })
}

export function PostNewExercise(dest, exercise, appState){
    return post(dest, exercise)
           .then((res) => {
             appState.exercises.push(res);
           })
}
