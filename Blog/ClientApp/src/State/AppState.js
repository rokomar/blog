import { decorate, observable } from 'mobx';

class AppState{
  workouts = [];
  date = 0;
  exercises = [];
  newExercises = [];
}

decorate(AppState, {
  workouts: observable,
  date: observable,
  exercises: observable,
  newExercises: observable
})

export const appState = new AppState();
