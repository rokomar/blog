import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route } from 'react-router-dom';
import { LoginContainer } from './Containers/LoginContainer';
import { RegistrationContainer } from './Containers/RegistrationContainer';
import { MainContainer } from './Containers/MainContainer';


function App(){
    return (
    <div>
        <BrowserRouter>
            <Route exact path="/" component={LoginContainer} />
            <Route path="/registration" component={RegistrationContainer} />
            <Route path="/workouts" component={MainContainer} />
        </BrowserRouter>
    </div>
    );
}

ReactDOM.render(<App />, document.getElementById('root'));
