import React, { useState } from 'react';
import { LoginInput } from '../Components/LoginInput';
import { LoginUser } from '../Services/UserService';
import { Header } from '../Components/Header';

export function LoginContainer(props){

    const [error, setError] = useState('');

    const handleSubmit = (content) => {
        const body = {
            'Username' : content.username,
            'Password' : content.password
        };
        LoginUser('api/user/login', body, props)
          .catch((err) => {
              setError(err);
          });
    };

    return (
      <div>
      <Header />
      <LoginInput
            submit={handleSubmit}
            error={error} />
      </div>
    );
}
