import React, { useState } from 'react';
import { RegisterInput } from '../Components/RegisterInput';
import { RegisterUser } from '../Services/UserService';
import { Header } from '../Components/Header';

export function RegistrationContainer(props){

  const [error, setError] = useState('');

  const handleSubmit = (content) => {
       var body = {
         "Username": content.username,
         "Email": content.email,
         "Password": content.password,
       }

       RegisterUser("api/user/register", body, props)
          .catch((err)=> {
             setError(err);
          });
  }

  return(
    <div>
    <Header />
    <RegisterInput
        submit={handleSubmit}
        error={error}/>
    </div>
  );
}
