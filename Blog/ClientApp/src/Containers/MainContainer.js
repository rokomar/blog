import React, { useEffect, useContext, useState } from 'react';
import { Header } from '../Components/Header';
import { Workout } from '../Components/Workout';
import { AddNewWorkout } from '../Components/AddNewWorkout';
import { AddNewExercise } from '../Components/AddNewExercise';
import { GetWorkouts, PostNewWorkout, GetExercises, PostNewExercise } from '../Services/WorkoutService';
import { AppContext } from '../State/AppContext';
import styles from '../Styles/MainContainer.module.css';
import { observer } from 'mobx-react';


function MainPage(props){

  const [workouts, setWorkouts] = useState([]);
  const [errorAddExercise, setErrorAddExercise] = useState(false);

  const appState  = useContext(AppContext);

  useEffect(() => {
    (async function() {
            await GetWorkouts("api/workout/getWorkouts/".concat(localStorage.getItem('Username')), appState, renderWorkouts)
                 .catch((error) => {
                   if(error.message === '401')
                     props.history.replace("/");
                   console.log(error.message);
                 });
            renderWorkouts();
        })();

        GetExercises("api/workout/getExercises/".concat(localStorage.getItem('Username')),appState)
            .catch((error) => {
              if(error.message === '401')
                props.history.replace("/");
              console.log(error.message);
            })
     }, []);

  const handleAddWorkout = () => {
       PostNewWorkout('api/workout/postWorkout', appState)
          .catch((error) => {
            if(error.message === '401')
              props.history.replace("/");
            console.log(error.message);
          })
  }

  const handleAddExercise = (res) => {
      if(appState.exercises.find(e => e === res.exercise))
        setErrorAddExercise(true);
      else{
        setErrorAddExercise(false);
        const exercise = {
          'name': res.exercise,
          'description': res.description,
          'username': localStorage.getItem('Username')
        };

        PostNewExercise('api/workout/addExercise',exercise, appState)
            .catch((error) => {
              if(error.message === '401')
                props.history.replace("/");
              console.log(error.message);
            })
          }
  }

  const renderWorkouts = () => {
    const workouts = [<div></div>];
    const area = "workout";
    for(var i = 0; i < appState.workouts.length; i++){
      workouts.push(
          <Workout data={i} appState={appState} />
      );
    }

    setWorkouts(workouts);
  }


  return(
      <div>
      {appState.workouts.length!==0 ?
        <div className={styles.container}>
          <div className={styles.header}>
            <Header />
          </div>

          {workouts}

          <div className={styles.addNew}>
            <AddNewWorkout appState={appState} handleSubmit={handleAddWorkout} />
          </div>

          <div className={styles.addExercise}>
            <AddNewExercise handleSubmit={handleAddExercise} error={errorAddExercise}/>
          </div>
        </div>
        : null
      }
      </div>
  );
}

export const MainContainer = observer(MainPage);
