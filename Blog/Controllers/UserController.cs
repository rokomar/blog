﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Blog.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace Blog.Controllers
{
    [ApiController]
    public class UserController : ControllerBase
    {
        //Omogucuje mi da koristin Configuration u svon kontroleru
        private IConfiguration _configuration { get; }

        public UserController(IConfiguration configuration)
        {
            _configuration = configuration;
        }


        [AllowAnonymous]
        [HttpPost]
        [Route("api/user/login")]
        public IActionResult Login([FromBody]Korisnik korisnik)
        {
            if (ValidateUser(korisnik) == true)
            {
                var tokenString = GenerateJSONWebToken(korisnik);
                return Ok(new { token = tokenString,
                                username = korisnik.Username});
            }
            else
                return BadRequest(new { response = "username already taken" });
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("api/user/register")]
        public IActionResult Register([FromBody]Korisnik korisnik)
        {
            if (CheckForRegistration(korisnik))
            {
                RegisterUser(korisnik);
                return Ok(new { response = "Success"});
            }
            else
                return BadRequest(new { response = "Fail"});
        }

        private string GenerateJSONWebToken(Korisnik korisnik)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"])); //stvaran kljuc za kriptiranje tokena
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256); //specificiran kako ce se kriptirat (algoritam)

            var claims = new[] {
                new Claim(JwtRegisteredClaimNames.Sub, korisnik.Username)
            };

            var token = new JwtSecurityToken(_configuration["Jwt:Issuer"],
                _configuration["Jwt:Audience"],
                claims, //payload
                expires: DateTime.Now.AddMinutes(120),
                signingCredentials: credentials); //specificiran podatke od tokena

            return new JwtSecurityTokenHandler().WriteToken(token); //ovo mi stvara token
        }

        private Boolean ValidateUser(Korisnik korisnik)
        {
            using (BlogContext db = new BlogContext())
            {
                if (db.Korisnik.Any(o => o.Username == korisnik.Username && o.Password == korisnik.Password))
                    return true;
                else
                    return false;
            }
        }

        private Boolean CheckForRegistration(Korisnik korisnik)
        {
            using (BlogContext db = new BlogContext())
            {
                if (db.Korisnik.Any(o => o.Username == korisnik.Username))
                    return false;
                else
                    return true;
            }
        }

        private void RegisterUser(Korisnik korisnik)
        {
            using (BlogContext db = new BlogContext())
            {
                db.Korisnik.Add(korisnik);
                db.SaveChanges();
            }
        }
    }
}