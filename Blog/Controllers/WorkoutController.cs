﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Blog.Models;
using Blog.Models.DTO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;

namespace Blog.Controllers
{
    [ApiController]
    public class WorkoutController : ControllerBase
    {
        private IMapper mapper { get; set; }

        public WorkoutController()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<WorkoutDTO, Workout>()
                .ForMember(dest => dest.Id, act => act.Ignore());
            });

            mapper = config.CreateMapper();    
        }

        
        [HttpGet]
        [Authorize]
        [Route("api/workout/getWorkouts/{username}")]
        public IActionResult getWorkouts(string Username)
        {
            using (BlogContext db = new BlogContext())
            {
                var dates = db.Workout.Where(w => w.Username == Username)
                                      .Select(w => w.Date)
                                      .Distinct()
                                      .OrderByDescending(date => date)
                                      .Take(4)
                                      .ToArray();


                List<Workout[]> workouts = new List<Workout[]>();

                foreach (var date in dates)
                {
                    workouts.Add(db.Workout.Where(w => w.Date == date)
                            .ToArray());
                }

                return Ok(workouts);
            }
        }
        


        [HttpPost]
        [Authorize]
        [Route("api/workout/postWorkout")]
        public IActionResult postWorkout([FromBody] List<WorkoutDTO> workouts)
        {
            using (BlogContext db = new BlogContext())
            {
                var id = db.Workout.Last().Id;
                foreach(WorkoutDTO workoutDTO in workouts)
                {
                    Workout workout = mapper.Map<WorkoutDTO, Workout>(workoutDTO);
                    workout.Id = ++id;
                    db.Workout.Add(workout);
                }

                db.SaveChanges();
                return Ok(workouts);
        
            }
        }

        [HttpPost]
        [Authorize]
        [Route("api/workout/checkExercise")]
        public IActionResult checkExercises([FromBody]String name)
        {
            using (BlogContext db = new BlogContext())
            {
                if (db.Exercise.Any(e => e.Name == name))
                        return Ok(new { exercise = name });

                return NotFound(new { response = "Exercise doesn't exist"});
            }
        }

        [HttpPost]
        [Authorize]
        [Route("api/workout/addExercise")]
        public IActionResult addExercise([FromBody]Exercise exercise)
        {
            using(BlogContext db = new BlogContext())
            {
                db.Exercise.Add(exercise);
                db.SaveChanges();
                return Ok(exercise);
            }
        }


        [HttpGet]
        [Authorize]
        [Route("api/workout/getExercises/{username}")]
        public IActionResult getExercises(String username)
        {
            using (BlogContext db = new BlogContext())
            {
                if (db.Exercise.Any(e => e.Username == username))
                {
                    var exercises = db.Exercise.Where(e => e.Username == username)
                                               .Select(e => e.Name)
                                               .ToArray();
                    return Ok(new { exercises });
                }
                else
                    return NotFound("No exercises");
            }
        }

    }
}