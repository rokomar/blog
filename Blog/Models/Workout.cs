﻿using System;
using System.Collections.Generic;

namespace Blog.Models
{
    public partial class Workout
    {
        public int Id { get; set; }
        public DateTime? Date { get; set; }
        public string Name { get; set; }
        public string SessionDescription { get; set; }
        public string Username { get; set; }

        public virtual Exercise NameNavigation { get; set; }
        public virtual Korisnik UsernameNavigation { get; set; }
    }
}
