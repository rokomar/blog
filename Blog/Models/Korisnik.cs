﻿using System;
using System.Collections.Generic;

namespace Blog.Models
{
    public partial class Korisnik
    {
        public Korisnik()
        {
            Exercise = new HashSet<Exercise>();
            Workout = new HashSet<Workout>();
        }

        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }

        public virtual ICollection<Exercise> Exercise { get; set; }
        public virtual ICollection<Workout> Workout { get; set; }
    }
}
