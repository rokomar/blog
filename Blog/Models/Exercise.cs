﻿using System;
using System.Collections.Generic;

namespace Blog.Models
{
    public partial class Exercise
    {
        public Exercise()
        {
            Workout = new HashSet<Workout>();
        }

        public string Name { get; set; }
        public string Description { get; set; }
        public string Username { get; set; }

        public virtual Korisnik UsernameNavigation { get; set; }
        public virtual ICollection<Workout> Workout { get; set; }
    }
}
