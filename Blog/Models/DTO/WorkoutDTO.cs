﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.Models.DTO
{
    public class WorkoutDTO
    {
        public DateTime? Date { get; set; }
        public string Name { get; set; }
        public string Username { get; set; }
        public string SessionDescription { get; set; }
    }
}
